
/**
  A person that can befriend and unfriend other persons.
*/
public class Person
{
  private String name;
  private String friends;
  private int numFriends;
  private Picture pic;
  private int picX, picY;

  /**
    Constructs a person with the given name.
    @param aName person's name.
  */
  public Person(String aName)
  {
    name    = aName;
    friends = "";
  }

  /**
    Constructs a person with the given name, custom picture and the coords for
    the picture placement.
    @param aName person's name.
    @param pictureName person's picture file.
    @param xCoord position for the person's picture when displayed.
    @param yCoord position for the person's picture when displayed.
  */
  public Person(String aName, String picName, int picX, int picY)
  {
    name = aName;
    pic = new Picture(picName);
    this.picX = picX;
    this.picY = picY;
    friends = "";

    drawSelf();
  }

  /**
    Gets the person's name.
    @return person's name.
  */
  public String name()
  {
    return name;
  }

  /**
    Adds the given person to the list of friends, increases the friends counter,
    and draws a connection between the both persons.
    @param friend person's new friend.
  */
  public void befriend(Person friend)
  {
    friends = friends + friend.name + " ";
    numFriends = numFriends + 1;

    drawFriend(friend);
  }

  /**
    Triggers adding the given person to the list of friends and adding this
    person to the given person's list of friends.
    @param friend person's new friend.
  */
  public void mutualBefriend(Person friend)
  {
    this.befriend(friend);
    friend.befriend(this);
  }

  /**
    Removes the given person from the list of friends, and decreases the friends
    counter.
    @param nonFriend person's new acquittance.
  */
  public void unfriend(Person nonFriend)
  {
    friends = friends.replace(nonFriend.name + " ", "");
    numFriends = numFriends - 1;
  }

  /**
    Triggers removing the given person from the list of friends and removing this
    person from the given person's list of friends.
    @param friend person's new acquittance.
  */
  public void mutualUnfriend(Person nonFriend)
  {
    this.unfriend(nonFriend);
    nonFriend.unfriend(this);
  }

  /**
    Gets the list of friends.
    @return person's friend list.
  */
  public String getFriends()
  {
    return friends;
  }

  /**
    Gets the number of friends.
    @return person's number for friends.
  */
  public int getNumberOfFriends()
  {
    return numFriends;
  }

  /**
    Displays the person's picture on a canvas.
  */
  public void drawSelf()
  {
    pic.translate(picX, picY);
    pic.draw();
  }

  /**
    Displays a given friend's pictures on a canvas with a line joining both
    persons.
  */
  public void drawFriend(Person friend)
  {
    SmallCircle node = new SmallCircle(picX, picY);
    node.fill();

    Line lineToFriend = new Line(picX, picY, friend.picX, friend.picY);
    lineToFriend.draw();
  }

  /**
    TODO Displays all the friends on an canvas.
    + Requires storing friends as dictionary of persons.
  */
  public void drawAllFriends(){}
}
