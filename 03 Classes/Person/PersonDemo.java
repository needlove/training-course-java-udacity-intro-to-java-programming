
public class PersonDemo
{
  public static void main(String[] args)
  {
    Person person1 = new Person("Sara", "sara.jpg", 10, 200);
    Person person2 = new Person("Cheng-Han", "cheng-han.png", 300, 0);
    Person person3 = new Person("Cay", "cay.gif", 250, 180);

    person1.befriend(person2);
    person1.befriend(person3);
  }
}
