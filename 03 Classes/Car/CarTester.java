
public class CarTester
{
  public static void main(String[] args)
  {
    Car car = new Car(50);

    // TODO: Add 20 gallons and drive 100 miles
    car.addGas(20);
    car.drive(100);

    // TODO: Print actual and expected gas level
    System.out.println("Expected gas level: 18.0");
    System.out.println("Actual gas level: " + car.getGasInTank());
  }
}
