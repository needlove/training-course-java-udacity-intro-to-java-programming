
/**
  A simple Circle class.
*/
public class Circle
{
  private double radius;
  private static double DEFAULT_RADIUS = 1.0;

  /**
    Creates a circle with a default radius of size 1.0;
  */
  public Circle()
  {
    radius = DEFAULT_RADIUS;
  }

  /**
    Creates a circle with the given radius.
    @param radius for the circle.
  */
  public Circle(double radius)
  {
    this.radius = radius;
  }

  /**
    Calculates the perimeter for the circle.
    @return perimeter value.
  */
  public double perimeter()
  {
    return 2 * Math.PI * radius;
  }
}
