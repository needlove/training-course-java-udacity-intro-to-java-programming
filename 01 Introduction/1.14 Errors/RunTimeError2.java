
/**
  A simple program that shows a rum-time error that throws an exception.
*/
public class RunTimeError2
{
  public static void main(String[] args)
  {
    // Run-Time Error
    // The program compiles but an exception occurs at execution
    // and terminates the program.
    System.out.println(1 / 0);
  }
}
