
/**
  A simple program that shows a run time error with an undesired output.
*/
public class RunTimeError1
{
  public static void main(String[] args)
  {
    // Run-Time Error (Logic Error)
    // The program compiles but the output is wrong.
    System.out.println("Hello, Wrld!");
  }
}
