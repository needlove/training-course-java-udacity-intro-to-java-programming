/**
  A simple program that shows a cimpile-time error.
*/
public class CompileTimeError
{
  public static void main(String[] args)
  {
    // Compile-Time Error (Syntax Error)
    // There is no 'ouch' variable.
    // The program wont even compile.
    System.ouch.println("Hello, World!");
  }
}
