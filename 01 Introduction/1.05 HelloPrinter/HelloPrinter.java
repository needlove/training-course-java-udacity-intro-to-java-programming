
/**
  A simple program that prints the "Hello, World!" message.
*/
public class HelloPrinter
{
  public static void main(String[] args)
  {
    System.out.println("Hello, World!");
  }
}
