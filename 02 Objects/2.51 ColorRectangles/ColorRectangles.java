
/**
  Simple program that shows how mutators and different references to an
  object affect the color of a rectangle.
*/
public class ColorRectangles
{
  public static void main(String[] args)
  {
    Rectangle box1 = new Rectangle(60,  90, 20, 30);
    Rectangle box2 = new Rectangle(80, 120, 20, 30);

    box1.setColor(new Color(255, 0, 0));
    box1.draw();
    box2.setColor(new Color(0, 255, 0));
    box2.fill();
  }
}
