
/**
  Simple program that shows how mutators and different references to an
  object affect the color of a rectangle.
*/
public class CopyingRectangles
{
  public static void main(String[] args)
  {
    Rectangle box1 = new Rectangle(5, 10, 60, 90);
    Rectangle box2 = box1;

    box1.setColor(Color.RED);
    box2.setColor(Color.BLUE);
    box1.fill();
  }
}
