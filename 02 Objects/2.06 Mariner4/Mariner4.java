
/**
  Gets the arrival date of the Mariner 4, having the launch day and the
  numbers of days traveled.
*/
public class Mariner4
{
  private static final int TOTAL_FLIGTH_DAYS = 228;

  public static void main(String[] args) {
    Day flightDay = new Day(1964, 11, 28);

    flightDay.addDays(TOTAL_FLIGTH_DAYS);
    System.out.println(flightDay.toString());
  }
}
